#!/bin/sed -Ef

1 {
  s/.*/--------\n/
  s/.*/&--------\n/
  s/.*/&--------\n/
  s/.*/&--------\n/
  s/.*/&--------\n/
  s/.*/&--------\n/
  s/.*/&--------\n/
  s/.*/&--------\n/
}

:backtrack_loop
  :placement_loop
    /-/ {
      s/.*/====\n&/
      H
      s/====\n//
      s/-/@/
    }
    /-/ ! {
      b end_placement_loop
    }

    t clear_placement_flag
    :clear_placement_flag

    :spread_marks_horizontally
    s/-([^\n]*@)/x\1/
    s/(@[^\n]*)-/\1x/
    t spread_marks_horizontally

    :spread_marks_vertically
    s/[-x](.{8}[@X])/X\1/
    s/([@X].{8})[-x]/\1X/
    t spread_marks_vertically

    s/X/x/g

    t clear_flag_vertical_marks
    :clear_flag_vertical_marks

    :spread_marks_diagonal
    s/[-x](.{7}[@X])/X\1/
    s/([@X].{7})[-x]/\1X/
    s/[-x](.{9}[@Y])/Y\1/
    s/([@Y].{9})[-x]/\1Y/
    t spread_marks_diagonal

    s/[XY]/x/g

    t clear_flag_diagonal_marks
    :clear_flag_diagonal_marks

    b placement_loop

  :end_placement_loop

  /(.*@){8}/ b solved

  :pop_loop
    g
    /^$/ b no_solution
    s/(.*)\n====\n([^=]*)/\1/
    x
    s/.*\n====\n([^=]*)/\1/
    s/-/x/
    /-/ ! b pop_loop

  b backtrack_loop

:solved
s/.*/Solved: \n&/
q

:no_solution
s/.*/No solution\n&/
q
