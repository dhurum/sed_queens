#!/bin/sed -Ef

# ./queens_linkedin.sed < field
#
# cat field
# aabbbCc
# abBdbbc
# abdddbe
# abbdbbe
# afbbbff
# fffgfff
# fffffff

1 {
  N
  N
  N
  N
  N
  N

  :setup_loop
    s/([[:upper:]])/#\1/

    t clear_setup_placement_flag
    :clear_setup_placement_flag

    :setup_spread_color
    s/#([[:alpha:]])(.*)\1/#\1\2"/I
    t setup_spread_color
    s/#[[:alpha:]]/#/
    s/"/'/g

    t setup_clear_flag_color_marks
    :setup_clear_flag_color_marks

    :setup_spread_marks_horizontally
    s/[[:alpha:]]([^\n]*#)/'\1/
    s/(#[^\n]*)[[:alpha:]]/\1'/
    t setup_spread_marks_horizontally

    :setup_spread_marks_vertically
    s/[[:alpha:]'](.{7}[#"])/"\1/
    s/([#"].{7})[[:alpha:]']/\1"/
    t setup_spread_marks_vertically

    s/"/'/g

    t setup_clear_flag_vertical_marks
    :setup_clear_flag_vertical_marks

    :setup_spread_marks_diagonal
    s/[[:alpha:]'](.{6}[#"])/"\1/
    s/([#"].{6})[[:alpha:]']/\1"/
    s/[[:alpha:]'](.{8}[#`])/`\1/
    s/([#`].{8})[[:alpha:]']/\1`/
    t setup_spread_marks_diagonal

    s/["`]/'/g

    t setup_clear_flag_diagonal_marks
    :setup_clear_flag_diagonal_marks

    s/#/@/

    /[[:upper:]]/ ! {
      b end_setup_loop
    }

    b setup_loop

  :end_setup_loop
}

p

:backtrack_loop
  :placement_loop
    s/.*/====\n&/
    H
    s/====\n//
    s/([[:alpha:]])/#\1/

    t clear_placement_flag
    :clear_placement_flag

    :spread_color
    s/#([[:alpha:]])(.*)\1/#\1\2"/
    t spread_color
    s/#[[:alpha:]]/#/
    s/"/'/g

    t clear_flag_color_marks
    :clear_flag_color_marks

    :spread_marks_horizontally
    s/[[:alpha:]]([^\n]*#)/'\1/
    s/(#[^\n]*)[[:alpha:]]/\1'/
    t spread_marks_horizontally

    :spread_marks_vertically
    s/[[:alpha:]'](.{7}[#"])/"\1/
    s/([#"].{7})[[:alpha:]']/\1"/
    t spread_marks_vertically

    s/"/'/g

    t clear_flag_vertical_marks
    :clear_flag_vertical_marks

    :spread_marks_diagonal
    s/[[:alpha:]'](.{6}[#"])/"\1/
    s/([#"].{6})[[:alpha:]']/\1"/
    s/[[:alpha:]'](.{8}[#`])/`\1/
    s/([#`].{8})[[:alpha:]']/\1`/
    t spread_marks_diagonal

    s/["`]/'/g

    t clear_flag_diagonal_marks
    :clear_flag_diagonal_marks

    s/#/@/

    /[[:alpha:]]/ ! {
      b end_placement_loop
    }

    b placement_loop

  :end_placement_loop

  /(.*@){7}/ b solved

  :pop_loop
    g
    /^$/ b no_solution
    s/(.*)\n====\n([^=]*)/\1/
    x
    s/.*\n====\n([^=]*)/\1/
    s/[[:alpha:]]/'/
    /[[:alpha:]]/ ! b pop_loop

  b backtrack_loop

:solved
s/'/./g
s/.*/Solved: \n&/
q

:no_solution
s/'/./g
s/.*/No solution\n&/
q
